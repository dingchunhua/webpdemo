//
//  WebPProtocol.m
//  WebPDemo
//
//  Created by 丁春华 on 2018/1/7.
//  Copyright © 2018年 dingch. All rights reserved.
//

#import "WebPProtocol.h"
#import <YYImage/YYImageCoder.h>

@interface WebPProtocol () <NSURLConnectionDelegate>

@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) YYImageDecoder *decoder;

@end

@implementation WebPProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request
{
    if ([NSURLProtocol propertyForKey:@"WebPProtocolHandledKey" inRequest:request]) {
        return NO;
    }

    NSString *ua = [request valueForHTTPHeaderField:@"User-Agent"];
    if ([ua containsString:@"AppleWebKit"]) {

        NSURL *url = [request URL];
        NSString * const requestFiletype = [[url pathExtension] lowercaseString];
        if ([@"webp" isEqualToString:requestFiletype]) {
            return YES;
        }
    }

    return NO;
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request
{
    return request;
}

+ (BOOL)requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b
{
    return [super requestIsCacheEquivalent:a toRequest:b];
}

- (void)startLoading
{
    NSLog(@"startLoading:%@", self.request.URL);
    
    NSMutableURLRequest *newRequest = [self.request mutableCopy];
    [NSURLProtocol setProperty:@YES forKey:@"WebPProtocolHandledKey" inRequest:newRequest];
    
    self.connection = [NSURLConnection connectionWithRequest:newRequest delegate:self];
}

- (void)stopLoading
{
    [self.connection cancel];
    self.connection = nil;
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(nonnull NSURLResponse *)response
{
    self.decoder = [[YYImageDecoder alloc] init];
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageAllowed];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(nonnull NSData *)data
{
    [self.decoder updateData:data final:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    UIImage *image = [self.decoder frameAtIndex:0 decodeForDisplay:YES].image;
    if (!image) {
        [self.client URLProtocol:self didFailWithError:error];
        return;
    }
    NSData *imagePNGData = UIImagePNGRepresentation(image);
    [self.client URLProtocol:self didLoadData:imagePNGData];
    
    self.decoder = nil;
    [self.client URLProtocolDidFinishLoading:self];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.client URLProtocol:self didFailWithError:error];
}

@end
