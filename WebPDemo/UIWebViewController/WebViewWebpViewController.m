//
//  WebViewWebpViewController.m
//  WebPDemo
//
//  Created by 丁春华 on 2018/1/7.
//  Copyright © 2018年 dingch. All rights reserved.
//

#import "WebViewWebpViewController.h"

@interface WebViewWebpViewController () <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation WebViewWebpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"加载中...";
    
    NSURL *url = [NSURL URLWithString:@"http://promotion.bl.com/nc/APP_HDGL20171226000004333_4653.html"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    
    [self.view addSubview:self.webView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.webView.frame = self.view.frame;
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad...");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidFinishLoad...");
    self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"webViewdidFailLoadWithError:%@", error);
}

#pragma mark - getters and setters
- (UIWebView *)webView
{
    if (_webView == nil) {
        _webView =[[UIWebView alloc] init];
        
        _webView.delegate = self;
    }
    return _webView;
}

@end
