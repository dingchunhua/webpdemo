//
//  WKWebViewWebpViewController.m
//  WebPDemo
//
//  Created by 丁春华 on 2018/1/7.
//  Copyright © 2018年 dingch. All rights reserved.
//

#import "WKWebViewWebpViewController.h"
#import <WebKit/WebKit.h>

@interface WKWebViewWebpViewController () <WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;

@end

@implementation WKWebViewWebpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"加载中...";
    
    NSURL *url = [NSURL URLWithString:@"http://promotion.bl.com/nc/APP_HDGL20171226000004333_4653.html"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    
    [self.view addSubview:self.webView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.webView.frame = self.view.frame;
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [webView evaluateJavaScript:@"document.title" completionHandler:^(NSString * _Nullable title, NSError * _Nullable error) {
        self.title = title;
    }];
}

#pragma mark - getters and setters
- (WKWebView *)webView
{
    if (_webView == nil) {
        WKWebViewConfiguration *webviewConfiguration = [[WKWebViewConfiguration alloc] init];
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:webviewConfiguration];
        
        _webView.navigationDelegate = self;
    }
    return _webView;
}

@end
