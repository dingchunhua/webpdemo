//
//  LocalWebViewController.m
//  WebPDemo
//
//  Created by 丁春华 on 2018/1/7.
//  Copyright © 2018年 dingch. All rights reserved.
//

#import "LocalWebViewController.h"

@interface LocalWebViewController () <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation LocalWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *htmlPath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:htmlPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
    [self.view addSubview:self.webView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.webView.frame = self.view.frame;
}

#pragma mark - getters and seters
- (UIWebView *)webView
{
    if (_webView == nil) {
        _webView = [[UIWebView alloc] init];
        
        _webView.delegate = self;
    }
    return _webView;
}

@end
