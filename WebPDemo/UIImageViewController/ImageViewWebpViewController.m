//
//  ImageViewWebpViewController.m
//  WebPDemo
//
//  Created by 丁春华 on 2018/1/7.
//  Copyright © 2018年 dingch. All rights reserved.
//

#import "ImageViewWebpViewController.h"
#import <YYWebImage/UIImageView+YYWebImage.h>

@interface ImageViewWebpViewController ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation ImageViewWebpViewController

#pragma mark - life cycles
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"UIImageView WebP";
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSURL *url = [NSURL URLWithString:@"http://img26.iblimg.com/content-4/images/webup/777264619.jpg.webp"];
    [self.imageView yy_setImageWithURL:url placeholder:nil];
    [self.view addSubview:self.imageView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.imageView.frame = CGRectMake(0, 0, self.view.frame.size.width, 300);
}

#pragma mark - getters and setters
- (UIImageView *)imageView
{
    if (_imageView == nil) {
        _imageView = [[UIImageView alloc] init];
    }
    return _imageView;
}

@end
