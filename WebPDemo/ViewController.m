//
//  ViewController.m
//  WebPDemo
//
//  Created by 丁春华 on 2018/1/7.
//  Copyright © 2018年 dingch. All rights reserved.
//

#import "ViewController.h"
#import "ImageViewWebpViewController.h"
#import "WebViewWebpViewController.h"
#import "WKWebViewWebpViewController.h"
#import "LocalWebViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation ViewController

#pragma mark - life cycles
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController = nil;
    
    if (indexPath.row == 0) {
        viewController = [[ImageViewWebpViewController alloc] init];
    } else if (indexPath.row == 1) {
        viewController = [[WebViewWebpViewController alloc] init];
    } else if (indexPath.row == 2) {
        viewController = [[WKWebViewWebpViewController alloc] init];
    } else if (indexPath.row == 3) {
        viewController = [[LocalWebViewController alloc] init];
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma makr - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark - getters and setters
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

- (NSArray *)dataSource
{
    if (_dataSource == nil) {
        _dataSource = @[
                        @"UIImageView加载WebP",
                        @"UIWebView加载WebP",
                        @"WKWebView加载WebP",
                        @"本地UIWebView"
                        ];
    }
    return _dataSource;
}

@end
